var PackService = require('./services/pack-service');
var packageTypes = require('./package-types');

/**
 * function for bakery test to calculate price and packages
 * @param {string } type , should be one of ['VS5','MB11','CF']
 * @param {integer} number 
 */
function CalcPrice(type, number) {

    let totalPrice=0;
    let packlist =[];
    let result;
    let packService = new PackService();
    let typePrices = [];
    let hasError = false;

    //if (validateNumber(number))

    switch (type) {
        case 'VS5':
            packService.packIt(number, packageTypes.VS5_Packs);
            typePrices = packageTypes.VS5_Prices;
            break;

        case 'MB11':
            packService.packIt(number, packageTypes.MB11_Packs);
            typePrices = packageTypes.MB11_Prices;
            break;

        case 'CF':
            packService.packIt(number, packageTypes.CF_Packs);
            typePrices = packageTypes.CF_Prices;
            break;
        default:
            console.log('wrong type');
            this.hasError = true;
            break;
    }

    if (!hasError) {
        if (!packService.hasError) {
            console.log(number, type);
    
            packlist = packService.getResult();
            if (packlist.get(0)) {
                let map1 = new Map([...packlist].slice(1));
                map1.forEach((value,key)=>{
                    totalPrice += value * typePrices.get(key);
                    console.log(`${value}*${key} ${typePrices.get(key)}`);
                })
                console.log(`Total Price: ${totalPrice.toFixed(2)} \n`);
                
            } else {
                console.log('can not properly assign packages\n');
            }
    
        } else {
            for (let e of packService.errors)
                console.log(e.message)
        }
    }   
    

}

module.exports = CalcPrice;