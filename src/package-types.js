
module.exports = {
    VS5_Prices: new Map([[3,6.99],[5,8.99]]),
    VS5_Packs : [5,3],
    MB11_Prices: new Map([[2,9.95],[5,16.95],[8,24.95]]),
    MB11_Packs: [8,5,2],
    CF_Prices: new Map([[3,5.95],[5,9.95],[9,16.99]]) ,
    CF_Packs: [9,5,3],
};