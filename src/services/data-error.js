/**
 * record error information
 * message: error message
 * data: data that causes error
 */

class DataError {
    constructor(message, data) {
        this.message = message;
        this.data = data;
    }
}

module.exports = DataError;