var DataError= require('./data-error.js');

/**
 * service to divide an integer value into factors
 * should use packIt method, then check hasError.
 * this.errors contains error information if hasError is true
 * return value is a Map, Map.get(0) is true, means the value can be divided successfully
 * Map.get(0) is false, means the value can not be divided properly
 */

class PackService {

    constructor(){
        this.result = new Map();
        this.result.set(0,false);
        this.errors =[];
        this.hasError = false;
    }


    /**
     * if value and factor are valid , then call calcPack() to get result.
     * should always use this method
     * @param {integer} value 
     * @param {int[]} factors 
     */
    packIt(value, factors) {
        if (this.validateValue(value) && this.validateFactors(factors)) {
            this.calcPack(value, factors);
        } else {
            this.hasError = true;
        }
    }

    /**
     * decompose value by factors, 
     * @param {integer} value  - the value , such as 10
     * @param {int[]} factors - the factors such as [7,3,2]     
     * the order of the element in this array represent the priority to use ,
     * so if 10 @ [7,3,2] = 1*7 + 1 * 3
     * and 10 @ [2,3,7] = 5*2
     */
      
    calcPack(value,factors) {        

        let currentFactor = factors[0];
        let left = value % factors[0];
        let currentValue = left;
        this.result.set(currentFactor,Math.floor(value/factors[0]));
    
        if ( left === 0) {
            this.result.set(0,true);
            return true; 
        } 
        else {
            if (factors.length >1) {
                let nextFactors = factors.slice(1);
                while (true){
                    if (!this.calcPack(currentValue, nextFactors)) {
                        let v = this.result.get(currentFactor)
                        if ( v>0) {
                            this.result.set(currentFactor,v-1);
                            currentValue+=currentFactor;
                        } else {
                            return false;
                        }
                    } else{
                        return true;
                    }
                }        
            } else {
                return false;
            }           
            
        }   
        
    }

    /**
     * validate value is integer and large than 0
     * @param {any} value 
     */
    validateValue(value) {
        let t = Number.parseInt(value);
        if(Number.isInteger(value) && value > 0) {
            return true;            
        } else {
            this.errors.push(new DataError(`invalid value`,value));
            return false;
        }
        
    }

    /**
     * validate factors is an array of integer and not include 0 
     * @param {[]} factors 
     */
    validateFactors(factors) {
        //return (!factors.some(isNaN) && !factors.includes(0))
        let f = factors.find(f=>!Number.isInteger(f));
        if (f!== undefined) {
            this.errors.push(new DataError('factors array should only contains integer',factors));
            return false;
        } 
        else if (factors.includes(0)) {
            this.errors.push(new DataError('factors array can not contain zero',factors));
            return false;
        }
        return true;
    }


    /**
     * return factors that has value other than 0
     */
    getResult() {        
        let map1 = new Map([...this.result].filter(([k,v]) => v !== 0 ));
        return map1;
    }
}



module.exports = PackService;
