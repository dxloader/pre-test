var chai = require('chai');
var expect = chai.expect;
var PackService = require('../src/services/pack-service.js');

describe('OrderPack',()=>{
    let packService;
    beforeEach(()=>{
        packService = new PackService();
    });

    it('10 VS5 should be success packed',()=>{
        //let packService = new PackService();

        packService.calcPack(10,[5,3]);
        expect(packService.getResult().get(0)).to.equal(true);
    });

    it('2 VS5 can not be success packed',()=>{
        //let packService = new PackService();

        packService.calcPack(2,[5,3]);
        expect(packService.getResult().get(0)).to.equal(false);
    });

    it('13 CF can be packed as 2*5 and 1*3',()=>{
        //let packService = new PackService();

        packService.calcPack(13,[9,5,3]);
        expect(packService.getResult().get(0)).to.equal(true);
        expect(packService.getResult().get(5)).to.equal(2);
        expect(packService.getResult().get(3)).to.equal(1);
    });

    it('14 MB11 can be packed as 1*8 and 3*2',()=>{
        //let packService = new PackService();

        packService.calcPack(14,[8,5,2]);
        expect(packService.getResult().get(0)).to.equal(true);
        expect(packService.getResult().get(8)).to.equal(1);
        expect(packService.getResult().get(2)).to.equal(3);
    });
})